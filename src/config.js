const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '49b8b586105622451a4bf3381c8f18cc';


const IMAGE_BASE_URL ='http://image.tmdb.org/t/p/';

const BACKDROP_SIZE = 'w1280';

// w92, w154, w185, w342, w500, w780, original
const POSTER_SIZE = 'w500';

export {
  API_URL,
  API_KEY,
  IMAGE_BASE_URL,
  BACKDROP_SIZE,
  POSTER_SIZE
}