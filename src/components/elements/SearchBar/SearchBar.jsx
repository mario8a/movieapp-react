import React, { useState } from 'react';
import './SearchBar.css';

export const SearchBar = ({setBusqueda}) => {

   const [searchTerm, setSearchTerm] = useState('');
   // const [error, setError] = useState(false);

   const buscarPelicula = e => {
      e.preventDefault();

      //Validate
      if(searchTerm.trim() === '') {
         // setError(true);
         return
      }
      // setError(false);

      //Send termino hacia componente principañ
      setBusqueda(searchTerm);
   }

   return (
      <>
         <form action="" className="form" onSubmit={buscarPelicula}>
            <input 
               type="text" 
               className="form-text" 
               placeholder="Buscar pelicula"
               onChange={e => setSearchTerm(e.target.value)}/>
            <input 
               type="submit" 
               className="btn" 
               value="Buscar" />
         </form>
      </>
   )
}
