import React from 'react';
import './HeroImage.css'

import { IMAGE_BASE_URL,
         BACKDROP_SIZE,
          } from '../../../config';

export const HeroImage = ({data}) => {
   const image = `${IMAGE_BASE_URL}${BACKDROP_SIZE}${data.backdrop_path}`;

   return (
      <div className="rmdb-heroimage"
            style={{
               background: `
                  linear-gradient(to bottom, rgba(0,0,0,0)
                  39%,rgba(0,0,0,0)
                  41%,rgba(0,0,0,0.65)
                  60%),
                  url('${image}'), #1c1c1c
               `
            }}>
         <div className="rmdb-heroimage-content">
            <div className="rmdb-heroimage-text">
               <h1> {data.title} </h1>
               <p> {data.overview} </p>
            </div>
         </div>
      </div>
   )
}
