import React from 'react';
import {calcTime, convertMoney} from '../../../helpers';
import './MovieInfoBar.css';

export const MovieInfoBar = ({time}) => {
   return (
      <div className="rmdb-movieinfobar">
         <div className="rmdb-movieinfobar-content">
            <div className="rmdb-movieinfobar-content-col">
               <i className="fas fa-clock fa-time"></i>
               <span className="rmdb-movieinfobar-info"> Duración: {calcTime(time.runtime)} </span>
            </div>
            <div className="rmdb-movieinfobar-content-col" >
               <i className="fas fa-money-bill-wave-alt fa-budget"></i>
               <span className="rmdb-movieinfobar-info">Presupuesto: {convertMoney(time.budget)} </span>
            </div>
            <div className="rmdb-movieinfobar-content-col" >
               <i className="fas fa-ticket-alt fa-revenue"></i>
               <span className="rmdb-movieinfobar-info"> Ingresos: {convertMoney(time.revenue)} </span>
            </div>
         </div>
      </div>
   )
}
