import React from 'react';
import './LoadMoreBtn.css';

export const LoadMoreBtn = ({text, onClick}) => {
   return (
      <div className="rmdb-loadmorebtn" onClick={onClick}>
        <p> {text} </p>
      </div>
   )
}
