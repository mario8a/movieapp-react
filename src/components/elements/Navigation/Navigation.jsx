import React from 'react';
import { Link } from 'react-router-dom';
import './Navigation.css';


export const Navigation = ({movie}) => {
   const {title} = movie;
   return (
      <div className="rmdb-navigation">
         <div className="rmdb-navigation-content">
            <Link to="/">
               <p>Home</p>
            </Link>
            <p>/</p>
            <p> {title}</p>
         </div>
      </div>
   )
}
