import React from 'react';
import { BACKDROP_SIZE, IMAGE_BASE_URL, POSTER_SIZE } from '../../../config';
import { MovieThumb } from '../MovieThumb/MovieThumb';

import './MovieInfo.css'

export const MovieInfo = ({directors, movie}) => {
   // console.log(;
   const image = `${IMAGE_BASE_URL}${BACKDROP_SIZE}${movie.backdrop_path}`;
   return (
      <div className="rmdb-movieinfo"
      style={{
         background: movie.backdrop_path ? `url('${image}')`: '#000'
      }}
      >
         <div className="rmdb-movieinfo-content">
            <div className="rmdb-movieinfo-thumb">
               <MovieThumb
                     image={movie.backdrop_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}` : './images/no_image.jpg' }
                     clickable={false}
                  />
            </div>
            <div className="rmdb-movieinfo-text">
               <h1>
                  {movie.title}
               </h1>
               <h3>PLOT</h3>
               <p> {movie.overview} </p>
               <h3>RATING</h3>
               <div className="rmdb-rating">
                  <meter min="0" max="100" optimum="100" low="40" high="70" value={movie.vote_average}></meter>
                  <p className="rmdb-score"> {movie.vote_average} </p>
               </div>
               {directors.length > 1 ? <h3>Director</h3> : <h3>Director</h3> }
               {directors.map((element, i) => {
                  return <p key={i} className="rmdb-director"> {element.name} </p>
               })}
            </div>
            <i className="fas fa-film"></i>
         </div>
      </div>
   )
}
