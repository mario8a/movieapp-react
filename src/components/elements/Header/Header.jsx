import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

export const Header = () => {
   return (
      <div className="rmdb-header">
         <div className="rmdb-header-content">
            <Link to="/" style={{ textDecoration: 'none' }}>
               <h2>React Movie</h2>
            </Link>
         </div>
      </div>
   )
}
