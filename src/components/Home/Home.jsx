import React, { useEffect, useState } from 'react';

// config
import { API_URL,
   API_KEY,
   POSTER_SIZE, 
   IMAGE_BASE_URL} from '../../config';
   
   //estilos
import './Home.css'
// components
import { FourColGrid } from '../elements/FoutColGrid/FourColGrid';
import { HeroImage } from '../elements/HeroImage/HeroImage';
import { LoadMoreBtn } from '../elements/LoadMoreBtn/LoadMoreBtn';
import { MovieThumb } from '../elements/MovieThumb/MovieThumb';
import { SearchBar } from '../elements/SearchBar/SearchBar';
import { Spinner } from '../elements/Spinner/Spinner';

export const Home = () => {

   const [movies, setMovies] = useState([]);
   const [heroImage, setHeroImage] = useState(null);
   const [loading, setLoading] = useState(false);
   const [currentPage, setCurrentPage] = useState(0);
   const [totalPages, setTotalPages] = useState(0);
   const [Busqueda, setBusqueda] = useState('');

   useEffect(() => {
      setLoading(true);
      const endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=es-US&page=1`;
      fetchData(endpoint)
   }, []);

   useEffect(() => {
      if(Busqueda === '') return;
      let endpoint = '';
      setLoading(true);

      if(Busqueda === '') {
         endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=es-US&page=1`;
      } else {
         endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=es-US&query=${Busqueda}`
      }
      fetchData(endpoint);

   }, [Busqueda]);

   
   const loadMoreItems = () => {
      let endpoint = '';
      setLoading(true);

      if(Busqueda === '') {
         endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=es-US&page=${currentPage + 1}`;
      } else {
         endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=es-US&query=${Busqueda}&page=${currentPage + 1}`
      }
      fetchData(endpoint);
   }

   const fetchData = async (endpoint) =>  {
      const data = await( await fetch(endpoint)).json();
      setMovies(data.results)
      setHeroImage(data.results[0])
      setLoading(false)
      setCurrentPage(data.page)
      setTotalPages(data.total_pages);
   }

   return (
      <div className="rmdb-home ">
         { 
            heroImage ?
               <div>
                  <HeroImage
                     data={heroImage}/>
                  <SearchBar setBusqueda={setBusqueda}/>
               </div> : null 
         }
         <div className="rmdb-home-grid">
            <FourColGrid
                  header={Busqueda ? 'Search Result' : 'Populares'}
                  loading={loading}
            >
               {movies.map((element, i) => {
                  return <MovieThumb
                           key={i}
                           clickable={true}
                           image={element.poster_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}${element.poster_path}` : `./images/no_image.jpg`}
                           movieId={element.id}
                           movieName={element.original_title}
                        />
               })}
            </FourColGrid>

            {
               loading ? <Spinner/> : null
            }

            { 
               (currentPage <= totalPages && !loading) ? 
                  <LoadMoreBtn text="Load more" onClick={loadMoreItems}/>
                  : null
            }
                  
         </div>
      </div>
   )
};