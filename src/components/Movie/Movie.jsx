import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
//Components
import { Navigation } from '../elements/Navigation/Navigation';
import { MovieInfo } from '../elements/Movieinfo/MovieInfo';
import { MovieInfoBar } from '../elements/MovieInfoBar/MovieInfoBar';
import { FourColGrid } from '../elements/FoutColGrid/FourColGrid';
import { Spinner } from '../elements/Spinner/Spinner';
// config
import { API_URL, API_KEY } from '../../config';
import './Movie.css'
import { Actor } from '../elements/Actor/Actor';

export const Movie = () => {

   const [movie, setMovie] = useState([]);
   const [actors, setActors] = useState([]);
   const [directors, setDirectors] = useState([]);
   const [loading, setLoading] = useState(false);

   const {movieId} = useParams();

   useEffect(() => {
      setLoading(true);
      //Fetch the movie
      window.scrollTo(0,0);
      const endpoint = `${API_URL}/movie/${movieId}?api_key=${API_KEY}&language=es-US`;
      fetchItems(endpoint)
   },[]);

   const fetchItems = async (endpoint) =>  {
      const data = await(await fetch(endpoint)).json();
      if(data.success) {
         setLoading(false)
      } else {
         setMovie(data)
         const creditsEndpoint = `${API_URL}/movie/${movieId}/credits?api_key=${API_KEY}`;
         const creditsData = await(await fetch(creditsEndpoint)).json();
         const directors = creditsData.crew.filter((member) => member.job === "Director");
         setDirectors(directors);
         setActors(creditsData.cast);
         setLoading(false);
      }
   }

   return (
      <div className="rmdb-movie">
         {
            movie ?
               <div>
                  <Navigation movie={movie} />
                  <MovieInfo movie={movie} directors={directors} />
                  <MovieInfoBar time={movie}/>
               </div>
               : null
         }
         {
            actors ?
               <div className="rmdb-movie-grid">
                  <FourColGrid header={'Actors'}>
                     { actors.map((element, i) => {
                        return <Actor key={i} actor={element}/>
                     }) }
                  </FourColGrid>
               </div>
               : null
         }
         { !actors && !loading ? <h1>No movie found!</h1> : null }
         { loading ? <Spinner/> : null}

      </div>
   )
}
