import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


//Components
import { Header } from '../elements/Header/Header';
import { NotFound } from '../elements/NotFound/NotFound';
import { Home } from '../Home/Home';
import { Movie } from '../Movie/Movie';

export const App = () => {
   return (
      <BrowserRouter>
            <Header/>
            <Switch>
               <Route exact path='/' component={Home} />
               <Route exact path='/:movieId' component={Movie} />
               <Route component={NotFound} />
            </Switch>
      </BrowserRouter>
   )
}
