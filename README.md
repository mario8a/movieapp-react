# React Movie


_Proyecto desarrollado con React que utiliza la API de themoviedb para mostrar las peliculas mas polulares que hay hoy en dia. Dentro de ella podras ver detalles de la pelicula al darle click sobre ella en el que encontraras la duracion, rating, presupuesto y actores que participaron en la pelicula que seleccionaste_

![Captura de Movie React](./.readme-static/react-movie.jpg)
![Captura de Movie React](./.readme-static/movie-react.jpg)

[Ver en netlify](https://naughty-tesla-05bc7a.netlify.app/)

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node js
```

### Instalación 🔧

_Clona el repositorio y una vez clonado, instalá las dependencias necesarias ejecutando el siguiente comando:_

```
npm install
```

_Levanta el proyecto con el siguiente comando:_

```
npm start
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [React](https://es.reactjs.org/) - El framework web usado
* [themoviedb](https://www.themoviedb.org/?language=es) - API usada para mostrar las peliculas


---
⌨️ con ❤️ por [Mario Ochoa](https://www.instagram.com/mario_8a_/) 😊
